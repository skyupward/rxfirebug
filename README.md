RXFireBug
=========
    
A really simple Bug tracking toy.
    
1. just basic bug process flow is supported
2. really simple bug statistics and admin
3. no performance, appearance, security is considered
4. can't guarantee no error in it
      
**Do not use it in product environment.**
    
default admin account:     user:Admin pwd:123456