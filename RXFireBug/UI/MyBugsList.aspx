﻿<%@ Page Title="my bugs" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyBugsList.aspx.cs" Inherits="RXFireBug.UI.MyBugsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured">
        <div class="content-wrapper">
            <nav>
                <ul id="menu">
                    <li><a id="A1" href="BugMeList.aspx?project=<%= ProjectID %>">Bug Me</a></li>
                    <li><a id="A3" href="MyBugsList.aspx?project=<%= ProjectID %>">My Bugs</a></li>
                    <li><a id="A4" href="AllBugs.aspx?project=<%= ProjectID %>">All Bugs</a></li>
                </ul>
            </nav>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Button ID="AddBugButtonTop" runat="server" Text="Add Bug" OnClick="AddBugButtonTop_Click" />
    <br />
    <asp:Label ID="Label1" runat="server" Text="Solved bugs" Font-Bold="true"></asp:Label>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" ShowFooter="True" GridLines="Vertical" CellPadding="10"
        ItemType="RXFireBug.Models.Bug" SelectMethod="GetSolvedMyBugs">
        <EmptyDataTemplate>
            No data!
        </EmptyDataTemplate>
        <Columns>
            <asp:BoundField DataField="BugID" HeaderText="ID" SortExpression="BugID" />
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <a href="BugDetail.aspx?bug=<%#: Item.BugID %>"><%#: Item.Name%></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Priority">
                <ItemTemplate>
                    <%#: Item.Priority%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="BugID" />
            <asp:BoundField DataField="DispatchTime" HeaderText="DispatchTime" SortExpression="BugID" />
            <asp:BoundField DataField="Version" HeaderText="Version" SortExpression="BugID" />
            <asp:BoundField DataField="Module" HeaderText="Module" SortExpression="BugID" />
            <asp:TemplateField HeaderText="Creater">
                <ItemTemplate>
                    <%#: Item.Creater%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Unsolved bugs" Font-Bold="true"></asp:Label>
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" ShowFooter="True" GridLines="Vertical" CellPadding="10"
        ItemType="RXFireBug.Models.Bug" SelectMethod="GetUnsolvedMyBugs">
        <EmptyDataTemplate>
            No data!
        </EmptyDataTemplate>
        <Columns>
            <asp:BoundField DataField="BugID" HeaderText="ID" SortExpression="BugID" />
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <a href="BugDetail.aspx?bug=<%#: Item.BugID %>"><%#: Item.Name%></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Priority">
                <ItemTemplate>
                    <%#: Item.Priority%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="BugID" />
            <asp:BoundField DataField="DispatchTime" HeaderText="DispatchTime" SortExpression="BugID" />
            <asp:BoundField DataField="Version" HeaderText="Version" SortExpression="BugID" />
            <asp:BoundField DataField="Module" HeaderText="Module" SortExpression="BugID" />
            <asp:TemplateField HeaderText="Dispatcher">
                <ItemTemplate>
                    <%#: Item.Dispatcher%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Processor">
                <ItemTemplate>
                    <%#: Item.Processor%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    <asp:Button ID="AddBugButtonBottom" runat="server" Text="Add Bug" OnClick="AddBugButtonBottom_Click" />
</asp:Content>
