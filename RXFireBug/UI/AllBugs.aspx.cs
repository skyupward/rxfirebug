﻿using RXFireBug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RXFireBug.UI
{
    public partial class AllBugs : System.Web.UI.Page
    {
        public int ProjectID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ProjectID = Convert.ToInt32(Request.QueryString["project"]);
        }

        public IQueryable<Bug> GetAllBugs()
        {

            var db = new DBContext();
            IQueryable<Bug> query = db.Bugs.Where(b => b.ProjectID == ProjectID);
            return query;
        }

        protected void AddBugButtonTop_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/CreateBug.aspx?project=" + ProjectID);
        }

        protected void AddBugButtonBottom_Click(object sender, EventArgs e)
        {
            AddBugButtonTop_Click(sender, e);
        }
    }
}