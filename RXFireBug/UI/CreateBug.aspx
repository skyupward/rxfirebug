﻿<%@ Page Title="create bug" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateBug.aspx.cs" Inherits="RXFireBug.UI.CreateBug" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured">
        <div class="content-wrapper">
            <nav>
                <ul id="menu">
                    <li><a id="A1" href="BugMeList.aspx?project=<%= ProjectID %>">Bug Me</a></li>
                    <li><a id="A3" href="MyBugsList.aspx?project=<%= ProjectID %>">My Bugs</a></li>
                    <li><a id="A4" href="AllBugs.aspx?project=<%= ProjectID %>">All Bugs</a></li>
                </ul>
            </nav>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">


    <asp:Table ID="Table1" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label  runat="server" Text="Name"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="NameTextBox" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label  runat="server" Text="Priority"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:RadioButtonList ID="PriorityRadioButtonList" runat="server">
                    <asp:ListItem Selected="True" Value="Low" >Low</asp:ListItem>
                    <asp:ListItem Value="Middle" >Middle</asp:ListItem>
                    <asp:ListItem Value="High" >High</asp:ListItem>
                </asp:RadioButtonList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Label3" runat="server" Text="Version"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:DropDownList ID="VersionDropDownList" runat="server"
                    ItemType="RXFireBug.Models.Version"
                    SelectMethod="GetVersion" DataTextField="Name"
                    DataValueField="Name">
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Label4" runat="server" Text="Module"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:DropDownList ID="ModuleDropDownList" runat="server"
                    ItemType="RXFireBug.Models.Module"
                    SelectMethod="GetModule" DataTextField="Name"
                    DataValueField="Name">
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Label5" runat="server" Text="Processor"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:DropDownList ID="UsersDropDownList" runat="server"
                    ItemType="RXFireBug.Models.Users"
                    SelectMethod="GetUsers" DataTextField="UserName"
                    DataValueField="UserName">
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="Label6" runat="server" Text="Describe"></asp:Label>
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="DescribeMultiLineTextBox" runat="server" TextMode="MultiLine" Rows="10"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="SubmitButton" runat="server" Text="Submit" OnClick="SubmitButton_Click" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="AddBugLabel" runat="server" Text=""></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
