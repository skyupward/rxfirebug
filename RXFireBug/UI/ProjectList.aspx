﻿<%@ Page Title="project list" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjectList.aspx.cs" Inherits="RXFireBug.UI.ProjectList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:GridView runat="server" AutoGenerateColumns="False" ShowFooter="True" GridLines="Vertical" CellPadding="10"
        ItemType="RXFireBug.Models.Project"  SelectMethod="GetProjects" >
        <Columns>
            <asp:BoundField DataField="ProjectID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <a href="BugMeList.aspx?project=<%#: Item.ProjectID %>"><%#: Item.Name%></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Creater">
                <ItemTemplate>
                    <%#: Item.Creater%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Create Time">
                <ItemTemplate>
                    <%#: Item.CreateTime%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
