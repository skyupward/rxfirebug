﻿<%@ Page Title="bug detail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BugDetail.aspx.cs" Inherits="RXFireBug.UI.BugDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
     <section class="featured">
        <div class="content-wrapper">
            <nav>
                <ul id="menu">
                    <li><a id="A1" href="BugMeList.aspx?project=<%= ProjectID %>">Bug Me</a></li>
                    <li><a id="A3" href="MyBugsList.aspx?project=<%= ProjectID %>">My Bugs</a></li>
                    <li><a id="A4" href="AllBugs.aspx?project=<%= ProjectID %>">All Bugs</a></li>
                </ul>
            </nav>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:FormView ID="productDetails" runat="server" ItemType="RXFireBug.Models.Bug" SelectMethod="GetBug" RenderOuterTable="false">
        <ItemTemplate>
            <div>
                <h1><%#:Item.Name %></h1>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <span><b>Version:</b>&nbsp;<%#: Item.Version %></span></td>
                    <td>
                        <span><b>Module:</b>&nbsp;<%#: Item.Module %></span></td>
                    <td>
                        <span><b>Priority:</b>&nbsp;<%#: Item.Priority %></span></td>

                </tr>

                <tr>


                    <td>
                        <span><b>Creater:</b>&nbsp;<%#: Item.Creater %></span></td>
                    <td>
                        <span><b>Processor:</b>&nbsp;<%#: Item.Processor %></span></td>
                    <td>
                        <span><b>Dispatcher:</b>&nbsp;<%#: Item.Dispatcher %></span></td>
                </tr>
                <tr>
                    <td>
                        <span><b>Dispatch Time:</b>&nbsp;<%#: Item.DispatchTime %></span></td>
                    <td>
                        <span><b>Status:</b>&nbsp;<%#: Item.Status %></span></td>
                    <td>
                        <span><b>Create Time:</b>&nbsp;<%#: Item.CreateTime %></span></td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>
    <h3>Flow</h3>
    <asp:ListView ID="ListView1" runat="server" ItemType="RXFireBug.Models.Dispatch" SelectMethod="GetDispatch" >
        <EmptyDataTemplate>
            No Flow!
        </EmptyDataTemplate>
        <ItemTemplate>
            <table>

                <tr>
                    <td>
                        <span><%#: Item.Status %>---(<%#: Item.Dispatcher %>--><%#: Item.Processor %>)---<%#: Item.Describe %>---<%#: Item.DispatchTime %></span></td>


                </tr>
            </table>
        </ItemTemplate>
    </asp:ListView>
    <br />
    <div runat="server" id="SolveBug">
        <asp:RadioButton ID="SolvedRadioButton" runat="server" Text="Solved" BorderStyle="None" Checked="True" EnableTheming="True" AutoPostBack="True" GroupName="solve bug" OnCheckedChanged="SolvedRadioButton_CheckedChanged" />
        <asp:RadioButton ID="DeliverRadioButton" runat="server" Text="Deliver" BorderStyle="None" AutoPostBack="True" GroupName="solve bug" OnCheckedChanged="DeliverRadioButton_CheckedChanged" />
        <asp:DropDownList ID="UsersDropDownList" runat="server"
            ItemType="RXFireBug.Models.Users"
            SelectMethod="GetUsers" DataTextField="UserName"
            DataValueField="UserName" Visible="False">
        </asp:DropDownList><br />
         
        <asp:Label ID="Label1" runat="server" Text="Describe" Font-Bold="true"></asp:Label>
        <br/>
        <asp:TextBox ID="DescribeTextBox" runat="server" Rows="10" TextMode="MultiLine"></asp:TextBox><br />
        <asp:Button ID="SubmitButton" runat="server" Text="Submit" OnClick="SubmitButton_Click" />
        <asp:Label ID="SolvedBugLabel" runat="server" Text=""></asp:Label>
    </div>
</asp:Content>
