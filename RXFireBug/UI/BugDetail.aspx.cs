﻿using RXFireBug.Biz;
using RXFireBug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RXFireBug.UI
{
    public partial class BugDetail : System.Web.UI.Page
    {
        public int ProjectID { get; set; }
        public int BugID { get; set; }
        public Bug Bug { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            BugID = Convert.ToInt32(Request.QueryString["bug"]);
            Bug = new DBContext().Bugs.SingleOrDefault(b => b.BugID == BugID);
            ProjectID = Bug.ProjectID;
            if (!Bug.Processor.Equals(User.Identity.Name))
            {
                SolveBug.Visible = false;
            }
            else
            {
                SolveBug.Visible = true;
            }

        }

        public IQueryable<Bug> GetBug(
                       [QueryString("bug")] int? bugId)
        {
            var dbc = new DBContext();
            IQueryable<Bug> query = dbc.Bugs;
            if (bugId > 0)
            {
                query = query.Where(b => b.BugID == bugId);
            }
            else
            {
                query = null;
            }
            return query;
        }

        public IQueryable<Users> GetUsers()
        {
            var db = new Entities();
            IQueryable<Users> query = db.Users;
            return query;
        }

        public IQueryable<Dispatch> GetDispatch([QueryString("bug")] int? bugId)
        {
            var db = new DBContext();
            IQueryable<Dispatch> query = db.Dispatches.Where(d => d.BugID == bugId);
            return query;
        }

        protected void DeliverRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (DeliverRadioButton.Checked)
            {
                UsersDropDownList.Visible = true;
            }
        }

        protected void SolvedRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (SolvedRadioButton.Checked)
            {
                UsersDropDownList.Visible = false;
            }
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            AddDispatch addDispatch = new AddDispatch();
            bool result = false;
            if (Bug != null && SolvedRadioButton.Checked)
            {
                result = addDispatch.Add(BugID, ReadOnly.Status.SOLVED, User.Identity.Name, "None", DescribeTextBox.Text);

                Bug.Describe = DescribeTextBox.Text;
                Bug.Processor = "None";
                Bug.Status = ReadOnly.Status.SOLVED;
                Bug.DispatchTime = DateTime.Now;
                Bug.Dispatcher = User.Identity.Name;

            }
            else if (Bug != null && DeliverRadioButton.Checked)
            {
                result = addDispatch.Add(BugID, ReadOnly.Status.UNSOLVED, User.Identity.Name, UsersDropDownList.SelectedValue, DescribeTextBox.Text);
                Bug.Describe = DescribeTextBox.Text;
                Bug.Processor = UsersDropDownList.SelectedValue;
                Bug.Status = ReadOnly.Status.UNSOLVED;
                Bug.DispatchTime = DateTime.Now;
                Bug.Dispatcher = User.Identity.Name;
            }

            if (result)
            {
                //string pageUrl = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Count() - Request.Url.Query.Count());
                //    Response.Redirect(pageUrl + "?ProductAction=add");
                if (!Bug.Processor.Equals(User.Identity.Name))
                {
                    SolveBug.Visible = false;
                }
                else
                {
                    SolveBug.Visible = true;
                }
                new DBContext().SaveChanges();
                //SolvedBugLabel.Text = "Success";
            }
            else
            {
                SolvedBugLabel.Text = "Faild";
            }

        }

    }
}