﻿using RXFireBug.Biz;
using RXFireBug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RXFireBug.UI
{
    public partial class CreateBug : System.Web.UI.Page
    {
        public int ProjectID { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ProjectID = Convert.ToInt32(Request.QueryString["project"]);
        }

        public IQueryable<RXFireBug.Models.Version> GetBug()
        {
            var db = new RXFireBug.Models.DBContext();
            IQueryable<RXFireBug.Models.Version> query = db.Versions;
            return query;
        }

        public IQueryable<RXFireBug.Models.Version> GetVersion()
        {
            var db = new DBContext();
            IQueryable<RXFireBug.Models.Version> query = db.Versions;
            return query;
        }

        public IQueryable<Module> GetModule()
        {
            var db = new DBContext();
            IQueryable<Module> query = db.Modules;
            return query;
        }

        public IQueryable<Users> GetUsers()
        {
            var db = new Entities();
            IQueryable<Users> query = db.Users;
            return query;
        }

        //public List<string> GetGroupMembers()
        //{
        //    //var db = new Entities();
        //    //Project project = db.Users.SingleOrDefault(p => p.ProjectID == Convert.ToInt32(Request.QueryString["project"]));
        //    //return project.GroupMembers;

        //}

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            string name = NameTextBox.Text;
            string priority = PriorityRadioButtonList.SelectedValue;
            string version = VersionDropDownList.SelectedValue;
            string module = ModuleDropDownList.SelectedValue;
            string user = UsersDropDownList.SelectedValue;
            string describe = DescribeMultiLineTextBox.Text;

            AddBug action = new AddBug();
            bool result = action.Add(ProjectID, name, priority, version, module, user, describe);

            if (result)
            {
                //string pageUrl = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Count() - Request.Url.Query.Count());
                //    Response.Redirect(pageUrl + "?ProductAction=add");
                AddBugLabel.Text = "Success";
            }
            else
            {
                AddBugLabel.Text = "Faild";
            }
        }
    }
}