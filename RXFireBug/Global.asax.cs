﻿using RXFireBug;
using RXFireBug.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace RXFireBug
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {

            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterOpenAuth();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            if (!System.Web.Security.Roles.RoleExists("Administrator"))
            {
                System.Web.Security.Roles.CreateRole("Administrator");
            }
            if (Membership.GetUser("Admin") == null)
            {
                Membership.CreateUser("Admin", "123123", "123@123.com");
                System.Web.Security.Roles.AddUserToRole("Admin", "Administrator");
            }

        }

        void Application_End(object sender, EventArgs e)
        {
          
        }

        void Application_Error(object sender, EventArgs e)
        {
           
        }
    }
}
