﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RXFireBug.Biz
{
    public struct ReadOnly
    {
        public struct Status
        {
            public const string SOLVED = "solved";
            public const string CLOSED = "closed";
            public const string UNSOLVED = "unsolved";
        }

        public struct Priority
        {
            public const string LOW = "low";
            public const string MIDDLE = "middle";
            public const string HIGH = "high";
        }
    }
}