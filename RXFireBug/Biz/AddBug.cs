﻿using RXFireBug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RXFireBug.Biz
{
    public class AddBug
    {
        public bool Add(int projectID, string name, string priority, string version, string module, string processor, string describe)
        {
            var bug = new RXFireBug.Models.Bug();
            bug.ProjectID = projectID;
            bug.Name = name;
            bug.Creater = HttpContext.Current.User.Identity.Name;
            bug.CreateTime = DateTime.Now;
            bug.Priority = priority;
            bug.Version = version;
            bug.Module = module;
            bug.Processor = processor;
            bug.Describe = describe;
            bug.Dispatcher = bug.Creater;
            bug.DispatchTime = bug.CreateTime;
            bug.Status = ReadOnly.Status.UNSOLVED;


            DBContext dbc = new DBContext();
            dbc.Bugs.Add(bug);
            dbc.SaveChanges();

            return true;

        }
    }
}