﻿using RXFireBug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RXFireBug.Biz
{
    public class AddDispatch
    {
        public bool Add(int bugID, string status, string dispatcher, string processor, string describe)
        {
            var dispatch = new RXFireBug.Models.Dispatch();
           dispatch.BugID = bugID;
           dispatch.Processor = processor;
           dispatch.Describe = describe;
           dispatch.Dispatcher =dispatcher;
           dispatch.DispatchTime = DateTime.Now;
           dispatch.Status = status;


            DBContext dbc = new DBContext();
            dbc.Dispatches.Add(dispatch);
            dbc.SaveChanges();

            return true;

        }
    }
}