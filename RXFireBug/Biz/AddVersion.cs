﻿using RXFireBug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RXFireBug.Biz
{
    public class AddVersion
    {
        public bool Add(string name)
        {
            var version = new RXFireBug.Models.Version();
            version.Name = name;
            version.Creater = HttpContext.Current.User.Identity.Name;
            version.CreateTime = DateTime.Now;
            

            DBContext dbc = new DBContext();
            dbc.Versions.Add(version);
            dbc.SaveChanges();

            return true;

        }
    }
}