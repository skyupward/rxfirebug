﻿using RXFireBug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RXFireBug.Biz
{
    public class AddModule
    {
        public bool Add(string name)
        {
            var module = new RXFireBug.Models.Module();
            module.Name = name;
            module.Creater = HttpContext.Current.User.Identity.Name;
            module.CreateTime = DateTime.Now;


            DBContext dbc = new DBContext();
            dbc.Modules.Add(module);
            dbc.SaveChanges();

            return true;

        }
    }
}