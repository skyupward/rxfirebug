﻿using RXFireBug.Biz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RXFireBug.Admin
{
    public partial class VersionAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public IQueryable<RXFireBug.Models.Version> GetVersions()
        {
            var db = new RXFireBug.Models.DBContext();
            IQueryable<RXFireBug.Models.Version> query = db.Versions;
            return query;
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            string name = VersionTextBox.Text;
            AddVersion action = new AddVersion();
            bool result = action.Add(name);

            if (result)
            {
                Response.Redirect(Request.Url.AbsolutePath);
            }
            else
            {
                AddVersionLabel.Text = "Faild";
            }

        }
    }
}