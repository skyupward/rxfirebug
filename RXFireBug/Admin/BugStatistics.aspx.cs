﻿using RXFireBug.Models;
using RXFireBug.Biz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RXFireBug.Admin
{
    public partial class BugStatistics : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public IQueryable<Project> GetProjects()
        {
            var db = new DBContext();
            IQueryable<Project> query = db.Projects;
            return query;
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            DateTime from = FromCalendar.SelectedDate;
            DateTime to = ToCalendar.SelectedDate;
            string projectName = ProjectDropDownList.SelectedItem.Text;
            int projectID = Convert.ToInt32(ProjectDropDownList.SelectedValue);
            TimeLabel.Text = string.Format("项目{0}---从 {1} 到 {2}", projectName, from.Date, to.Date);
            var db = new DBContext();
            int allBugs = db.Bugs.Count(b => b.ProjectID == projectID && b.CreateTime >= from && b.CreateTime <= to);
            int solvedBugs = db.Bugs.Count(b => b.ProjectID == projectID && b.CreateTime >= from && b.CreateTime <= to && b.Status == ReadOnly.Status.SOLVED);
            int unsolvedBugs = db.Bugs.Count(b => b.ProjectID == projectID && b.CreateTime >= from && b.CreateTime <= to && b.Status == ReadOnly.Status.UNSOLVED);
            AllLabel.Text = " " + allBugs;
            SolvedLabel.Text = " " + solvedBugs;
            UnsovedLabel.Text = " " + unsolvedBugs;
            StatisticsInfo.Visible = true;
        }


    }
}