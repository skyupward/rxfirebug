﻿<%@ Page Title="statistics" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BugStatistics.aspx.cs" Inherits="RXFireBug.Admin.BugStatistics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured">
        <div class="content-wrapper">
            <nav>
                <ul id="menu">
                    <li><a id="A1" runat="server" href="~/Admin/ProjectAdmin.aspx">ProjectAdmin</a></li>
                    <li><a id="A4" runat="server" href="~/Admin/ModuleAdmin.aspx">ModuleAdmin</a></li>
                    <li><a id="A3" runat="server" href="~/Admin/VersionAdmin.aspx">VersionAdmin</a></li>
                    <li><a id="A6" runat="server" href="~/Admin/BugStatistics.aspx">Bug Statistics</a></li>
                </ul>
            </nav>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="Label6" runat="server" Text="Project: " Font-Bold="true"></asp:Label>
    <asp:DropDownList ID="ProjectDropDownList" runat="server"
        ItemType="RXFireBug.Models.Project"
        SelectMethod="GetProjects" DataTextField="Name"
        DataValueField="ProjectID"/>
        <asp:Table ID="Table1" runat="server">
            <asp:TableRow>
                <asp:TableCell>
                from
            </asp:TableCell>
                <asp:TableCell>
                    <asp:Calendar ID="FromCalendar" runat="server"></asp:Calendar>
                </asp:TableCell>
                <asp:TableCell>
                  to
            </asp:TableCell>
                <asp:TableCell>
                    <asp:Calendar ID="ToCalendar" runat="server"></asp:Calendar>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="SearchButton" runat="server" Text="Search" OnClick="SearchButton_Click" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <br />
        <div runat="server" id="StatisticsInfo" visible="false">
            <asp:Label ID="TimeLabel" runat="server" Text=""></asp:Label>
            <br />
            <asp:Label ID="Label1" runat="server" Text="All Bugs：" Font-Bold="true"></asp:Label><asp:Label ID="AllLabel" runat="server" Text=""></asp:Label>
            <br />
            <asp:Label ID="Label3" runat="server" Text="Soveld Bugs：" Font-Bold="true"></asp:Label><asp:Label ID="SolvedLabel" runat="server" Text=""></asp:Label>
            <br />
            <asp:Label ID="Label5" runat="server" Text="Unsolved Bugs：" Font-Bold="true"></asp:Label><asp:Label ID="UnsovedLabel" runat="server" Text=""></asp:Label>
        </div>
</asp:Content>
