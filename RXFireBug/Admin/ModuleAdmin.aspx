﻿<%@ Page Title="module admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ModuleAdmin.aspx.cs" Inherits="RXFireBug.Admin.ModuleAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <section class="featured">
        <div class="content-wrapper">
            <nav>
                <ul id="menu">
                    <li><a id="A1" runat="server" href="~/Admin/ProjectAdmin.aspx">ProjectAdmin</a></li>
                    <li><a id="A4" runat="server" href="~/Admin/ModuleAdmin.aspx">ModuleAdmin</a></li>
                    <li><a id="A3" runat="server" href="~/Admin/VersionAdmin.aspx">VersionAdmin</a></li>
                    <li><a id="A6" runat="server" href="~/Admin/BugStatistics.aspx">Bug Statistics</a></li>
                </ul>
            </nav>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:GridView  runat="server" AutoGenerateColumns="False" ShowFooter="True" GridLines="Vertical" CellPadding="10"
        ItemType="RXFireBug.Models.Module" SelectMethod="GetModules" >
        <Columns>
            <asp:BoundField DataField="ModuleID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Name">
                <ItemTemplate>
                    <%#: Item.Name%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Creater">
                <ItemTemplate>
                    <%#: Item.Creater%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Create Time">
                <ItemTemplate>
                    <%#: Item.CreateTime%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    <asp:Label ID="Label1" runat="server" Text="Module name"></asp:Label>
    <asp:TextBox ID="ModuleTextBox" runat="server"></asp:TextBox><asp:Button ID="AddButton" runat="server" Text="Add" OnClick="AddButton_Click" />
    <asp:Label ID="AddModuleLabel" runat="server" Text=""></asp:Label>
</asp:Content>
