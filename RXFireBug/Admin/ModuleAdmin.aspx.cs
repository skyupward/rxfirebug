﻿using RXFireBug.Biz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RXFireBug.Admin
{
    public partial class ModuleAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public IQueryable<RXFireBug.Models.Module> GetModules()
        {
            var db = new RXFireBug.Models.DBContext();
            IQueryable<RXFireBug.Models.Module> query = db.Modules;
            return query;
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            string name = ModuleTextBox.Text;
            AddModule action = new AddModule();
            bool result = action.Add(name);

            if (result)
            {
                Response.Redirect(Request.Url.AbsolutePath);
            }
            else
            {
                AddModuleLabel.Text = "Faild";
            }
        }
    }
}