﻿using RXFireBug.Biz;
using RXFireBug.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RXFireBug.Admin
{
    public partial class ProjectAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public IQueryable<Project> GetProjects()
        {
            var db = new RXFireBug.Models.DBContext();
            IQueryable<Project> query = db.Projects;
            return query;
        }


        protected void AddProjectButton_Click(object sender, EventArgs e)
        {
            string projectName = ProjectNameTextBox.Text;
           
            AddProject action = new AddProject();
            bool result = action.Add(projectName);

            if (result)
            {
                Response.Redirect(Request.Url.AbsolutePath);
            }
            else
            {
                AddProjectLabel.Text = "Faild";
            }


        }
    }
}