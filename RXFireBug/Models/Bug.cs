﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RXFireBug.Models
{
    public class Bug
    {
        public int BugID { get; set; }

        public int ProjectID { get; set; }

        public virtual Project Project { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public DateTime CreateTime { get; set; }

        public string Creater { get; set; }

        [Required]
        public string Version { get; set; }

        [Required]
        public string Module { get; set; }

        public string Status { get; set; }

        [Required]
        public string Priority { get; set; }

        [Required]
        public string Describe { get; set; }

        [Required]
        public string Processor { get; set; }

        public virtual ICollection<Dispatch> Dispatches { get; set; }

        [Required]
        public string Dispatcher { get; set; }

        public DateTime DispatchTime { get; set; }

    }
}