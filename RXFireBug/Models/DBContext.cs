﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RXFireBug.Models
{
    public class DBContext : DbContext
    {
        public DBContext()
            : base("RXFireBug")
        {
        }

        public DbSet<Bug> Bugs { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Version> Versions { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Dispatch> Dispatches { get; set; }

    }
}