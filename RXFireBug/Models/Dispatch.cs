﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RXFireBug.Models
{
    public class Dispatch
    {
        public int DispatchID { get; set; }

        public int BugID { get; set; }

        public virtual Bug Bug { get; set; }

        public string Describe { get; set; }

        public string Dispatcher { get; set; }

        public string Processor { get; set; }

        public string Status { get; set; }

        public DateTime DispatchTime { get; set; }
    }
}