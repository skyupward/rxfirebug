﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RXFireBug.Models
{
    public class Version
    {
        public int VersionID { get; set; }

        [Required]
        [StringLength(10)]
        public string Name { get; set; }

        public string Creater { get; set; }

        public DateTime CreateTime { get; set; }
    }
}