﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RXFireBug.Models
{
    public class Module
    {
        public int ModuleID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public string Creater { get; set; }

        public DateTime CreateTime { get; set; }
    }
}